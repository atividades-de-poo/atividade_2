package programacao_orientada_a_objetos.atividade_2.controller;

import programacao_orientada_a_objetos.atividade_2.model.Pessoa;
import programacao_orientada_a_objetos.atividade_2.view.pessoa.*;
import java.util.HashMap;
import java.util.ArrayList;

public class PessoaController {
  public static void index() {
		var pessoas = Pessoa.todos();
		new Index(new ArrayList(pessoas)).setVisible(true);
  }

  public static void create() {
		new Create().setVisible(true);
  }

  public static void store(HashMap<String, String> dados) {
		Pessoa.inserir(dados);
		var pessoa = Pessoa.encontrar(dados.get("cpf"));
		new Show(pessoa).setVisible(true);
  }

  public static void show(String cpf) {
		var pessoa = Pessoa.encontrar(cpf);
		new Show(pessoa).setVisible(true);
  }
  
  public static void edit(String cpf) {
		var pessoa = Pessoa.encontrar(cpf);
		new Update(pessoa).setVisible(true);
  }
  
  public static void update(HashMap<String, String> dados) {
		Pessoa.atualizar(dados);
		var pessoa = Pessoa.encontrar(dados.get("cpf"));
		new Show(pessoa).setVisible(true);
  }
}
