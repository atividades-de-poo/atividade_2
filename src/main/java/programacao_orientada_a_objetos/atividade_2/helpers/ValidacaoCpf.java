/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package programacao_orientada_a_objetos.atividade_2.helpers;

import java.util.HashSet;
import java.util.regex.Pattern;

/**
 *
 * @author ciro
 */
public class ValidacaoCpf {
	public static boolean validar(String cpf) {
		return naoNulo(cpf)       &&
			temOnzeCaracteres(cpf)  &&
			todosSaoDigitos(cpf)    &&
			todosSaoDiferentes(cpf) &&
			digitosSaoValidos(cpf);
	}

	private static boolean naoNulo(String cpf) {
		return cpf != null;
	}

	private static boolean temOnzeCaracteres(String cpf) {
		return cpf.length() == 11;
	}

	private static boolean todosSaoDiferentes(String cpf) {
    var invalidos = new HashSet<String>();
		var padroes   = new String[] {
			"00000000000",
			"11111111111",
			"22222222222",
			"33333333333",
			"44444444444",
			"55555555555",
			"66666666666",
			"77777777777",
			"88888888888",
			"99999999999"
		};
		for (var padrao: padroes) invalidos.add(padrao);
		return ! invalidos.contains(cpf);
	}

	private static boolean todosSaoDigitos(String cpf) {
		var formato = "[0-9]{11}";
		var padrao  = Pattern.compile(formato);
		var ambos   = padrao.matcher(cpf);
		return ambos.matches();
	}

	private static boolean digitosSaoValidos(String cpf) {
		return passaPrimeiroTeste(cpf) && passaSegundoTeste(cpf);
	}

	private static boolean passaPrimeiroTeste(String cpf) {
		var verificador = Character.getNumericValue(cpf.charAt(9));
		var digitos     = cpf.substring(0, 9).toCharArray();
		var sequencia   = new int[] {10, 9, 8, 7, 6, 5, 4, 3, 2};
		var soma        = 0;

		for (int i = 0; i < digitos.length; i++) {
			var digito = digitos[i];
			var numero = Character.getNumericValue(digito);
			var multiplicacao = numero * sequencia[i];
			soma += multiplicacao;
		}

		var resto = (soma * 10) % 11;
		if (resto == 10) resto = 0;
		return verificador == resto;
	}

	private static boolean passaSegundoTeste(String cpf) {
		var verificador = Character.getNumericValue(cpf.charAt(10));
		var digitos     = cpf.substring(0, 10).toCharArray();
		var sequencia   = new int[] {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
		var soma        = 0;

		for (int i = 0; i < digitos.length; i++) {
			var digito = digitos[i];
			var numero = Character.getNumericValue(digito);
			var multiplicacao = numero * sequencia[i];
			soma += multiplicacao;
		}

		var resto = (soma * 10) % 11;
		if (resto == 10) resto = 0;
		return verificador == resto;
	}
}
