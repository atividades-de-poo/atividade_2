package programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados;

import programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados.conexao.Conexao;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Query {
	private String                  tabela;
	private HashMap<String, String> ondes;
	private HashMap<String, String> atualizacoes;
	private HashSet<String>         colunas;

	public Query() {
		this.tabela       = "";
		this.colunas      = new HashSet<String>();
		this.ondes        = new HashMap<String, String>();
		this.atualizacoes = new HashMap<String, String>();
	}

	public static Query para(String tabela) {
		Query builder  = new Query();
		builder.tabela = tabela;
		return builder;
	}

	public Query onde(String coluna, String valor) {
		this.ondes.put(coluna, valor);
		return this;
	}

	public Query colunas(String coluna) {
		this.colunas.add(coluna);
		return this;
	}

	public Query atualizacao(String coluna, String valor) {
		this.atualizacoes.put(coluna, valor);
		return this;
	}

	public String todos() {
		return this.criarQuerySelect();
	}

	public String inserir(HashMap<String, String> dados) {
		return this.criarQueryInsert(dados);
	}

	public String atualizar() {
		return this.criarQueryUpdate();
	}

	private String criarQuerySelect() {
		var inicial = "select $colunas from $tabela $ondes";
		var colunas = this.criarColunas();
		var tabela  = this.tabela;
		var ondes   = this.criarOndes();

		return inicial.replace("$colunas", colunas)
			.replace("$tabela", tabela)
			.replace("$ondes", ondes);
	}

	private String criarColunas() {
		return this.colunas.size() == 0 ?
			"*":
			String.join(", ", this.colunas);
	}

	private String criarOndes() {
		if (this.ondes.size() == 0) return "";

		var ondes = new HashSet<String>();
		for (var coluna: this.ondes.keySet()) {
			var valor = this.ondes.get(coluna);
			ondes.add(coluna + " = " + valor);
		}
		return "where " + String.join(" and ", ondes);
	}

	private String criarQueryInsert(HashMap<String, String> dados) {
		var inicial = "insert into $tabela ($colunas) values ($valores)";
		var tabela  = this.tabela;
		var colunas = new ArrayList<String>();
		var valores = new ArrayList<String>();

		for (var linha: dados.entrySet()) {
			colunas.add(linha.getKey());
			valores.add(linha.getValue());
		}
		return inicial.replace("$tabela", tabela)
			.replace("$colunas", String.join(", ", colunas))
			.replace("$valores", String.join(", ", valores));
	}

	private String criarQueryUpdate() {
		var inicial      = "update $tabela $atualizacoes $ondes";
		var atualizacoes = this.criarAtualizacoes();
		var tabela       = this.tabela;
		var ondes        = this.criarOndes();

		return inicial.replace("$tabela", tabela)
			.replace("$atualizacoes", atualizacoes)
			.replace("$ondes", ondes);
	}

	private String criarAtualizacoes() {
		var atualizacoes = new HashSet<String>();
		for (var coluna: this.atualizacoes.keySet()) {
			var valor = this.atualizacoes.get(coluna);
			atualizacoes.add(coluna + " = " + valor);
		}
		return "set " + String.join(", ", atualizacoes);
	}
}
