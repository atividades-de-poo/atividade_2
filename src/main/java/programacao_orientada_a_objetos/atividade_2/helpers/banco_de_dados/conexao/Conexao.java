package programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao
{
    private static Connection conexao = null;

    public static Connection obter()
    {
        return (conexao == null) ?
            Conexao.tentar():
            conexao;
    }

    private static Connection tentar()
    {
        try {
            var url     = Conexao.url();
            System.out.println(url);
            var usuario = PropriedadesConexao.obter("bd.usuario");
            var senha   = PropriedadesConexao.obter("bd.senha");
            conexao = DriverManager.getConnection(url, usuario, senha);
        } catch (SQLException excecao) {
            conexao = null;
        } finally {
            return conexao;
        }
    }

    private static String url()
    {
        var host    = PropriedadesConexao.obter("bd.host");
        var driver  = PropriedadesConexao.obter("bd.driver");
        var dominio = PropriedadesConexao.obter("bd.dominio");
        var porta   = PropriedadesConexao.obter("bd.porta");
        var nome    = PropriedadesConexao.obter("bd.nome");

        return String.format(
            "%s:%s://%s:%s/%s",
            host, driver, dominio, porta, nome
        );
    }
}
