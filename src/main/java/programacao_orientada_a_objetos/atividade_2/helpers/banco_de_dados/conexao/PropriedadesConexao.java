package programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados.conexao;

import java.util.Properties;
import java.io.IOException;

public class PropriedadesConexao
{
    private static Properties propriedades = null;

    private static void carregar()
    {
        try {
            propriedades = new Properties();
            var loader   = PropriedadesConexao.class.getClassLoader();
            var arquivo  = "application.properties";
            var conteudo = loader.getResourceAsStream(arquivo);
            propriedades.load(conteudo);
            conteudo.close();
        } catch (IOException excecao) {
            propriedades = null;
        }
    }

    public static String obter(String chave)
    {
        if (propriedades == null)
            PropriedadesConexao.carregar();

        return (propriedades != null) ?
            propriedades.getProperty(chave).replaceAll("\"", ""):
            null;
    }

}
