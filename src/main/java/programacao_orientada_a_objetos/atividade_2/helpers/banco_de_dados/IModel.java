/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados;

import java.util.HashMap;

/**
 *
 * @author ciro
 */
public interface IModel {
  public String                  tabela();
  public HashMap<String, String> dadosDAO();
	public static String           Tabela() { return ""; }
}
