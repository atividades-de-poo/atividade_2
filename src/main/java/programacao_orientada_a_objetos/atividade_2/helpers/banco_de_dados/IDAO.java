/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados;

import java.util.HashMap;
import java.util.List;

/**
 *
 * @author ciro
 */
public interface IDAO<Modelo> {
	public List<Modelo> listar(String query);
	public void         inserir(String query);
	public Modelo       encontrar(String chave);
	public void         atualizar(String query);
}
