/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package programacao_orientada_a_objetos.atividade_2.model;

import programacao_orientada_a_objetos.atividade_2.helpers.ValidacaoCpf;

/**
 *
 * @author ciro
 */
public class Cpf {
  private String digitos;
  private String formatado;

	public Cpf(String digitos) {
		this.setDigitos(digitos);
		this.formatado = null;
	}


	public String getDigitos() {
		return this.digitos;
	}
	public void setDigitos(String digitos) {
		var validos  = ValidacaoCpf.validar(digitos);
		this.digitos = (validos) ? digitos : null;
	}


	public String getFormatado() {
		var digitos = this.getDigitos();
		if (digitos != null) {
			this.setFormatado(digitos);
		}
		return this.formatado;
	}
	private void setFormatado(String digitos) {
		var primeira      = digitos.substring(0,  3);
		var segunda       = digitos.substring(3,  6);
		var terceira      = digitos.substring(6,  9);
		var verificadores = digitos.substring(9, 11);

		this.formatado = String.format("%s.%s.%s-%s",
			primeira,
			segunda,
			terceira,
			verificadores
		);
	}

	@Override
	public String toString() {
    return this.getFormatado();
	}
}
