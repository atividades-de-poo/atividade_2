package programacao_orientada_a_objetos.atividade_2.model.dao;

import programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados.conexao.Conexao;
import programacao_orientada_a_objetos.atividade_2.model.Pessoa;
import java.util.ArrayList;
import java.util.List;
import java.sql.SQLException;
import programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados.IDAO;

public class PessoaDAO
	implements IDAO<Pessoa>
{
	public List<Pessoa> listar(String query) {
    var pessoas = new ArrayList<Pessoa>();
    try {
      var resultados = Conexao.obter().prepareStatement(query).executeQuery();
      resultados.beforeFirst();
      while (resultados.next()) pessoas.add(Pessoa.fromResultSet(resultados));
    } catch (SQLException excecao) {}
		return pessoas;
	}

	public void inserir(String query) {
		try {
			Conexao.obter().prepareStatement(query).execute();
		} catch (SQLException excecao) {}
	}

	public Pessoa encontrar(String chave) {
		var query = Pessoa.onde("cpf", "\"" + chave + "\"").todos();
		return this.listar(query).get(0);
	}

	public void atualizar(String query) {
		try {
			Conexao.obter().prepareStatement(query).execute();
		} catch (SQLException excecao) {}
	}
}
