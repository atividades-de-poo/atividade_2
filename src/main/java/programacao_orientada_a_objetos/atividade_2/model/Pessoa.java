/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package programacao_orientada_a_objetos.atividade_2.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados.Query;
import programacao_orientada_a_objetos.atividade_2.helpers.ValidacaoCpf;
import programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados.IQuery;
import programacao_orientada_a_objetos.atividade_2.helpers.banco_de_dados.IModel;
import programacao_orientada_a_objetos.atividade_2.model.dao.PessoaDAO;

/**
 *
 * @author ciro
 */
public class Pessoa
	implements IQuery, IModel
{
  private String nome;
  private Cpf    cpf;

	public Pessoa() {
    this.nome = "";
    this.cpf  = new Cpf("");
	}

  public void setNome(String nome) {
    this.nome = nome;
  }
  public String getNome() {
    return this.nome;
  }
  public void setCpf(String cpf) {
    this.cpf.setDigitos(cpf);
  }
  public String getCpf() {
    return this.cpf.toString();
  }
  public String getCpfParaEdicao() {
    return this.cpf.getDigitos();
  }

  public static Pessoa fromResultSet(ResultSet dados) {
    var pessoa = new Pessoa();
      try {
        pessoa.setNome(dados.getString("nome"));
        pessoa.setCpf(dados.getString("cpf"));
      } catch (SQLException ex) {}
    return pessoa;
  }

	@Override // IModel
  public String tabela() {
		return Pessoa.Tabela();
	}
  public static String Tabela() {
		return "pessoas";
	}
	@Override // IModel
  public HashMap<String, String> dadosDAO() {
    var dados = new HashMap<String, String>();
    dados.put("nome", "\"" + this.nome             + "\"");
    dados.put("cpf" , "\"" + this.cpf.getDigitos() + "\"");
    return dados;
  }

  public static List todos() {
		var query = Query.para(Tabela()).todos();
		return new PessoaDAO().listar(query);
	}
	public static void inserir(HashMap<String, String> dados) {
    var pessoa = new Pessoa();
    pessoa.setCpf(dados.get("cpf"));
    pessoa.setNome(dados.get("nome"));
		var query = Query.para(Tabela()).inserir(pessoa.dadosDAO());
		new PessoaDAO().inserir(query);
	}
	public static void atualizar(HashMap<String, String> dados) {
		var query = Query.para(Tabela())
      .onde("cpf", "\"" + dados.get("cpf") + "\"")
      .atualizacao("nome", "\"" + dados.get("nome") + "\"")
      .atualizar();
		new PessoaDAO().atualizar(query);
	}
	public static Query onde(String coluna, String valor) {
		return Query.para(Tabela()).onde(coluna, valor);
	}
	public static Query colunas(String coluna) {
		return Query.para(Tabela()).colunas(coluna);
	}
	public static Pessoa encontrar(String cpf) {
		return new PessoaDAO().encontrar(cpf);
	}

}
